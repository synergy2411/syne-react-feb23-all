import React from 'react';
import ReactDOM from 'react-dom/client';
import "bootstrap/dist/css/bootstrap.min.css";
import { createBrowserRouter, RouterProvider } from 'react-router-dom';

import './index.css';
import App, { RootLoader } from './App';
import Products from './Components/pages/Products';
import Courses from './Components/pages/Courses';
import ErrorPage from './Components/pages/ErrorPage';

const router = createBrowserRouter([
  {
    path: "/",
    element: <App />,
    errorElement: <ErrorPage />,
    loader: RootLoader,
    children: [
      {
        path: "/products",
        element: <Products />
      }, {
        path: "/courses",
        element: <Courses />
      }
    ]
  },
])

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <RouterProvider router={router}></RouterProvider>
);
