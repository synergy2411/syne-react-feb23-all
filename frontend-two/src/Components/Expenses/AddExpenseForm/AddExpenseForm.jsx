import React, { useState } from "react";
import { v4 } from "uuid";

const AddExpenseForm = (props) => {
  const [inputTitle, setInputTitle] = useState("");
  const [inputAmount, setInputAmount] = useState("");
  const [inputDate, setInputDate] = useState("");

  const titleChangeHandler = (event) => setInputTitle(event.target.value);
  const amountChangHandler = (event) => setInputAmount(event.target.value);
  const dateChangHandler = (event) => setInputDate(event.target.value);

  const cancelHandler = () => props.onClose();

  const handleSubmit = (event) => {
    event.preventDefault();
    let newExpense = {
      id: v4(),
      title: inputTitle,
      amount: Number(inputAmount),
      createdAt: new Date(inputDate),
    };
    props.onAddNewExpense(newExpense);
  };

  return (
    <div className="card">
      <div className="card-body">
        <h5 className="text-center">Add New Expense</h5>
        <form onSubmit={handleSubmit}>
          {/* title */}
          <div className="form-group mb-3">
            <label htmlFor="title">Title :</label>
            <input
              type="text"
              className="form-control"
              name="title"
              onChange={titleChangeHandler}
            />
          </div>
          {/* amount */}
          <div className="form-group mb-3">
            <label htmlFor="amount">Amount :</label>
            <input
              type="number"
              min="0.1"
              step="0.1"
              className="form-control"
              onChange={amountChangHandler}
            />
          </div>
          {/* date */}
          <div className="form-group mb-3">
            <input
              type="date"
              min="2020-04-01"
              max="2023-03-31"
              className="form-control"
              onChange={dateChangHandler}
            />
          </div>
          {/* buttons */}
          <div className="form-group">
            <div className="row">
              <div className="col-6">
                <div className="d-grid">
                  <button type="submit" className="btn btn-primary">
                    Add
                  </button>
                </div>
              </div>
              <div className="col-6">
                <div className="d-grid">
                  <button className="btn btn-light" onClick={cancelHandler}>
                    Cancel
                  </button>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
};

export default AddExpenseForm;
