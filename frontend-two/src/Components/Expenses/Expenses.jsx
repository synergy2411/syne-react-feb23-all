import React, { useState } from "react";
import AddExpenseForm from "./AddExpenseForm/AddExpenseForm";
import ExpenseFilter from "./ExpenseFilter/ExpenseFilter";
import ExpenseItem from "./ExpenseItem/ExpenseItem";

const INITIAL_EXPENSES = [
  {
    id: "e001",
    title: "shopping",
    amount: 19,
    createdAt: new Date("Dec 20, 2022"),
  },
  {
    id: "e002",
    title: "grocery",
    amount: 21,
    createdAt: new Date("Jan 2, 2023"),
  },
  {
    id: "e003",
    title: "insurance",
    amount: 29,
    createdAt: new Date("Aug 21, 2021"),
  },
  {
    id: "e004",
    title: "planting",
    amount: 40,
    createdAt: new Date("Oct 24, 2022"),
  },
];

const Expenses = () => {
  console.log("EXPENSES RENDER");

  const [expenses, setExpenses] = useState(INITIAL_EXPENSES);
  const [showAddExpense, setShowAddExpense] = useState(false);
  const [selectedYear, setSelecteYear] = useState("2023");

  const addExpenseClickHandler = () => setShowAddExpense(!showAddExpense);
  const onClose = () => setShowAddExpense(false);

  const onAddNewExpense = (expense) => {
    setExpenses((prevExpenses) => [expense, ...prevExpenses]);
    setShowAddExpense(false);
  };

  const onDeleteExpense = (expenseId) => {
    setExpenses((prevExpense) =>
      prevExpense.filter((exp) => exp.id !== expenseId)
    );
  };

  const onYearChange = (selYear) => setSelecteYear(selYear);

  let filteredExpenses = expenses.filter(
    (exp) => exp.createdAt.getFullYear().toString() === selectedYear
  );

  return (
    <div className="container">
      <h2 className="text-center">Expenses App</h2>
      <div className="row mb-3">
        <div className="col-4 offset-4">
          <div className="d-grid">
            <button className="btn btn-dark" onClick={addExpenseClickHandler}>
              Show Form
            </button>
          </div>
        </div>
        <div className="col-4">
          <ExpenseFilter yearChange={onYearChange} />
        </div>
      </div>

      <div className="row mb-3">
        <div className="col-6 offset-3">
          {showAddExpense && (
            <AddExpenseForm
              onAddNewExpense={onAddNewExpense}
              onClose={onClose}
            />
          )}
        </div>
      </div>

      <div className="row">
        {filteredExpenses.map((expense) => (
          <ExpenseItem
            expense={expense}
            key={expense.id}
            deleteExpense={onDeleteExpense}
          />
        ))}
      </div>
    </div>
  );
};

export default Expenses;
