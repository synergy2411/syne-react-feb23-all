import React from "react";
import { useRouteError } from "react-router-dom";

const ErrorPage = () => {
  const error = useRouteError();
  console.log(error);
  return (
    <div>
      <h6>Something went wrong...</h6>
      <p>
        {error.error.message} - {error.statusText}
      </p>
    </div>
  );
};

export default ErrorPage;
