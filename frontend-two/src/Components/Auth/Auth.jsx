import React, { useState, useRef, useContext } from "react";
import classes from "./Auth.module.css";
// import "./Auth.css";
import AuthContext from "../../context/auth-context";
import { hashSync, compareSync } from "bcryptjs";

const Auth = () => {
  const [inputUsername, setInputUsername] = useState("");
  const [usernameIsValid, setUsernameIsValid] = useState(true);
  const inputPasswordRef = useRef();

  const context = useContext(AuthContext);

  const usernameChangeHandler = (event) => setInputUsername(event.target.value);

  const submitHandler = (event) => {
    event.preventDefault();
    fetch(`http://localhost:3001/users?username=${inputUsername}`)
      .then((resp) => resp.json())
      .then((result) => {
        if (result.length > 0) {
          let isMatch = compareSync(
            inputPasswordRef.current.value,
            result[0].password
          );
          if (isMatch) {
            console.log("Authenticated User");
            context.setIsLoggedIn(true);
          } else {
            alert("NOT AUTHENTICATED");
          }
        } else {
          alert("Uanble to find user - " + inputUsername);
        }
      });
  };

  const usernameBlurHandler = () => {
    if (inputUsername.length === 0) {
      setUsernameIsValid(false);
    } else if (!inputUsername.includes("@")) {
      setUsernameIsValid(false);
    } else {
      setUsernameIsValid(true);
    }
  };

  const registerClickHandler = () => {
    fetch(`http://localhost:3001/users`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        username: inputUsername,
        password: hashSync(inputPasswordRef.current.value, 12),
      }),
    })
      .then((resp) => resp.json())
      .then((result) => {
        console.log("[RESULT]", result);
      })
      .catch(console.log);
  };

  return (
    <div className="container">
      <div className="row">
        <div className="col-6 offset-3">
          <div className="card">
            <div className="card-body">
              <h2>User is {context.isLoggedIn ? "" : "NOT"} Logged In!!</h2>
              <form onSubmit={submitHandler}>
                {/* username - Controlled Element*/}
                <div className="form-group mb-4">
                  <label htmlFor="username">Username:</label>
                  <input
                    type="text"
                    name="username"
                    className={`form-control ${
                      !usernameIsValid && classes["input-username-invalid"]
                    } `}
                    value={inputUsername}
                    onChange={usernameChangeHandler}
                    onBlur={usernameBlurHandler}
                  />

                  {usernameIsValid ? null : <p>Some Problem with username</p>}
                </div>
                {/* password - Uncontrolled Element*/}
                <div className="form-group mb-4">
                  <label htmlFor="password">Password :</label>
                  <input
                    type="password"
                    name="password"
                    className="form-control"
                    ref={inputPasswordRef}
                  />
                </div>
                {/* buttons */}
                <div className="form-group">
                  <button className="btn btn-primary" type="submit">
                    Login
                  </button>

                  <button
                    className="btn btn-secondary"
                    onClick={registerClickHandler}
                  >
                    Register
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Auth;
