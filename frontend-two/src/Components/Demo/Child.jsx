import React from "react";

const Child = ({ toggle }) => {
  console.log("[CHILD]");
  return (
    <div>
      <h5>The Child Component</h5>
      {toggle && <p>This content will be toggled</p>}
    </div>
  );
};

export default React.memo(Child);

// prevProps.toggle === currProps.toggle
// if toggle value is same as previous one, React memo does not re-render the child component

// prevProps.demoFn === currProps.demoFn
