import React, { useReducer } from "react";

const reducerFn = (prevState, action) => {
  if (action.type === "INCREMENT") {
    return {
      ...prevState,
      count: prevState.count + 1,
    };
  }
  if (action.type === "DECREMENT") {
    return {
      ...prevState,
      count: prevState.count - 1,
    };
  }
  if (action.type === "STORE_RESULT") {
    return {
      ...prevState,
      result: [prevState.count, ...prevState.result],
    };
  }
  return prevState;
};

const UseReducerDemo = () => {
  const [state, dispatch] = useReducer(reducerFn, { count: 0, result: [] });

  return (
    <div className="container text-center">
      <p className="display-4">Count : {state.count}</p>
      <button
        className="btn btn-primary"
        onClick={() => dispatch({ type: "INCREMENT" })}
      >
        Increase
      </button>
      <button
        className="btn btn-secondary"
        onClick={() => dispatch({ type: "DECREMENT" })}
      >
        Decrease
      </button>
      <br />
      <button
        className="btn btn-outline-danger"
        onClick={() => dispatch({ type: "STORE_RESULT" })}
      >
        Store Result
      </button>

      <ul>
        {state.result.map((r) => (
          <li key={r}>{r}</li>
        ))}
      </ul>
    </div>
  );
};

export default UseReducerDemo;
