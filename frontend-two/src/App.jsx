import { Fragment } from "react";
import { Link, Outlet, useLoaderData } from "react-router-dom";
import "./App.css";

const fetchTodos = () => {
  return new Promise((resolve, reject) => {
    fetch("http://localhost:3001/todos")
      .then((resp) => resp.json())
      .then((data) => resolve(data))
      .catch((err) => reject(err));
  });
};

export async function RootLoader() {
  try {
    const todoCollection = await fetchTodos();
    return { todoCollection };
  } catch (err) {
    console.error(err);
  }
}

function App() {
  const { todoCollection } = useLoaderData();
  console.log(todoCollection);
  return (
    <div className="container">
      <ul>
        <li>
          <Link to="/courses">Courses</Link>
        </li>
        <li>
          <Link to="/products">Products</Link>
        </li>
        <li>
          <Link to="/employee">Employee</Link>
        </li>
      </ul>
      <h1> App Component Loaded</h1>
      <Outlet />
    </div>
  );
}

export default App;
